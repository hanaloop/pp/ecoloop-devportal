#!/usr/bin/python

# Use conda for setting your python environment
# conda create -n webtest python=3.9
# conda activate webtest
# python -m pip install requests

import sys
import os
import requests
import json

ECOLOOP_BASE_URL = "https://www.ecoloop.one"

api_key = os.environ['ECOLOOP_API_KEY']

class ActivityDetails:
    def __init__(self, region: str, periodStartDt: str, inputAmount: float, inputAmountUnitId: str, activitySourceCode: str, activityTypeCode: str):
      self.region = region
      self.periodStartDt = periodStartDt
      self.inputAmount = inputAmount
      self.inputAmountUnitId = inputAmountUnitId
      self.activitySourceCode = activitySourceCode
      self.activityTypeCode = activityTypeCode

def estimate_emission(activity: ActivityDetails):
  """Function that fetches emission factor
  Parameters
  ----------
  query_criteria: str
  """
  api_url = ECOLOOP_BASE_URL + "/bapi/ghga/estimate-emissions"

  headers = {
    "Content-Type": "application/json; charset=utf-8",
    "x-api-key": api_key
  }

  response = requests.post(api_url, headers=headers, data=json.dumps(activity.__dict__))
  return response


# Main procedure
amount = float(sys.argv[1]) if len(sys.argv) > 1 else 1000

activity = ActivityDetails('kr', '2022-01-01', amount, 'VL-L', '0020', '2002')

print("Input: ", json.dumps(activity.__dict__));

response = estimate_emission(activity)
print("Status Code", response.status_code)

calc_result = response.json()

# Extracting name, activityTypeId, emissionSourceSink.activitySourceCode and calorificDenominatorUnitId
print("EmissionsPerGas: ", str(calc_result['emissionsPerGas']))

