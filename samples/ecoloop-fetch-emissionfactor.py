#!/usr/bin/python

# Use conda for setting your python environment
# conda create -n webtest python=3.9
# conda activate webtest
# python -m pip install requests

import sys
import os
import requests

ECOLOOP_BASE_URL = "https://www.ecoloop.one"

api_key = os.environ['ECOLOOP_API_KEY']


def fetch_emissionfactors(query_criteria):
  """Function that fetches emission factor
  Parameters
  ----------
  query_criteria: str
  """
  api_url = ECOLOOP_BASE_URL + "/bapi/ghga/emissionfactors?id:contains=" + query_criteria

  headers = {
    "Content-Type": "application/json; charset=utf-8",
    "x-api-key": api_key
  }

  response = requests.get(api_url, headers=headers)
  return response


# Main procedure
search_id = sys.argv[1] if len(sys.argv) > 1 else 'gasoline'
print ("Search for:", search_id)
response = fetch_emissionfactors(search_id)
print("Status Code", response.status_code)

emission_factors = response.json()
print("Response length: ", len(emission_factors));

# Extracting name, activityTypeId, emissionSourceSink.activitySourceCode and calorificDenominatorUnitId
for emission_factor in emission_factors:
  print("* name              : ", emission_factor['name'])
  print("  activityTypeId    : ", emission_factor['activityTypeId'])
  print("  activitySourceCode: ", emission_factor['emissionSourceSink']['activitySourceCode'])
  print("  inputAmountUnitId : ", emission_factor['calorificDenominatorUnitId'])

