import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

type FeatureItem = {
  title: string;
  Svg: React.ComponentType<React.ComponentProps<'svg'>>;
  description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
  {
    title: 'Manage Greenhouse gases',
    Svg: require('@site/static/img/undraw_pie_graph_re_fvol.svg').default,
    description: (
      <>
        Easily calculate and track Greenhouse gas emissions.
      </>
    ),
  },
  {
    title: 'Collaborate',
    Svg: require('@site/static/img/undraw_team_up_re_84ok.svg').default,
    description: (
      <>
        Collaborate with different stakeholders to achieve the common goal: Net-zero.
      </>
    ),
  },
  {
    title: 'Strategize',
    Svg: require('@site/static/img/undraw_landscape_photographer_re_2jrj.svg').default,
    description: (
      <>
        Based on data, make optimal plans for decarbonization, and execute them.
      </>
    ),
  },
];

function Feature({title, Svg, description}: FeatureItem) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
