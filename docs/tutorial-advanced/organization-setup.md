---
sidebar_position: 1
---

# Organization Setup

It is possible to store organization's setup in EcoLoop.

Organization setup is an hierarchical structure that allows tracking of activities at different levels of the organization.

In EcoLoop the setup is structured in four levels as follows:

```
├─ Organization
│  ├─ Organization Site (OrgSite)
│     ├─ Site Facility (OrgFacility)
│        ├─ Emission Source (OrgFacilityEmissionSource)
```

Each OrgFacilityEmissionSource is associated with the ActivitySourceCode and ActivityType, which you learned in the basic tutorial.

Once you have setup the Organization, you can add `ActivityLog`s to  OrgFacilityEmissionSources.

When ActivityLog is added or updated, EcoLoop automatically calculates the emission and updates the record.

As ActivityLog data is accumulated, you can leverage the reporting and forecasting services to generate report and forecast future value based on the historic data. 

**More content to be added...**