---
sidebar_position: 6
---

# Sample code

Here are some sample code in Python.

We suggest you to use `conda` for managing your python environments.

## Fetching EmissionFactors

```python title=ecoloop-fetch-emissionfactor.py
#!/usr/bin/python

import sys
import os
import requests

ECOLOOP_BASE_URL = "https://www.ecoloop.one"

api_key = os.environ['ECOLOOP_API_KEY']


def fetch_emissionfactors(query_criteria):
  """Function that fetches emission factor
  Parameters
  ----------
  query_criteria: str
  """
  api_url = ECOLOOP_BASE_URL + "/bapi/ghga/emissionfactors?id:contains=" + query_criteria

  headers = {
    "Content-Type": "application/json; charset=utf-8",
    "x-api-key": api_key
  }

  response = requests.get(api_url, headers=headers)
  return response


# Main procedure
search_id = sys.argv[1] if len(sys.argv) > 1 else 'gasoline'
print ("Search for:", search_id)
response = fetch_emissionfactors(search_id)
print("Status Code", response.status_code)

emission_factors = response.json()
print("Response length: ", len(emission_factors));

# Extracting name, activityTypeId, emissionSourceSink.activitySourceCode and calorificDenominatorUnitId
for emission_factor in emission_factors:
  print("* name              : ", emission_factor['name'])
  print("  activityTypeId    : ", emission_factor['activityTypeId'])
  print("  activitySourceCode: ", emission_factor['emissionSourceSink']['activitySourceCode'])
  print("  inputAmountUnitId : ", emission_factor['calorificDenominatorUnitId'])

```

# Estimating Emissions

```python title=ecoloop-fetch-emissionfactor.py
#!/usr/bin/python

import sys
import os
import requests
import json

ECOLOOP_BASE_URL = "https://www.ecoloop.one"

api_key = os.environ['ECOLOOP_API_KEY']

class ActivityDetails:
    def __init__(self, region: str, periodStartDt: str, inputAmount: float, inputAmountUnitId: str, activitySourceCode: str, activityTypeCode: str):
      self.region = region
      self.periodStartDt = periodStartDt
      self.inputAmount = inputAmount
      self.inputAmountUnitId = inputAmountUnitId
      self.activitySourceCode = activitySourceCode
      self.activityTypeCode = activityTypeCode

def estimate_emission(activity: ActivityDetails):
  """Function that fetches emission factor
  Parameters
  ----------
  query_criteria: str
  """
  api_url = ECOLOOP_BASE_URL + "/bapi/ghga/estimate-emissions"

  headers = {
    "Content-Type": "application/json; charset=utf-8",
    "x-api-key": api_key
  }

  response = requests.post(api_url, headers=headers, data=json.dumps(activity.__dict__))
  return response


# Main procedure
amount = float(sys.argv[1]) if len(sys.argv) > 1 else 1000

activity = ActivityDetails('kr', '2022-01-01', amount, 'VL-L', '0020', '2002')

print("Input: ", json.dumps(activity.__dict__));

response = estimate_emission(activity)
print("Status Code", response.status_code)

calc_result = response.json()

# Extracting name, activityTypeId, emissionSourceSink.activitySourceCode and calorificDenominatorUnitId
print("EmissionsPerGas: ", str(calc_result['emissionsPerGas']))

```