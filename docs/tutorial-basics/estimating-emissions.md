---
sidebar_position: 4
---

# Estimating Emissions

You finally arrived to the Estimating Emission tutorial! Good for you!

The request schema is as follows, where bold are the lookup data you retrieved from prior section.


| Field | Type | Req. | Description | Example |
|:-----|------|------|------|------|
| amount | positive number | Yes | The activity amount (rate) | 34.2 |
| calc Tiers | Json | No |The tier (advanced option) needed for calculation parameter  | JSON value |
| **activity Type** | string | Yes | The type of activity.   | "combustion-stat-solid" |
| region | string | Yes | The country code where activity occurred.  EmissionFactor may differ depending on the region.   | "kr" |
| **activity Source** (lookup)| string | Yes | The activity's emission source, e.g.  This data is the main filter for choosing the EmissionFactor  | "0011" (crude oil) |
| **amount Unit Id** (lookup)| string | Yes | The unit of the input amount | "VL-L" (volume: liter) |
| periodStartDt | date | Yes | The time when activity occurred. EmissionFactor may differ depending on the region  | "2022-03-04" |
| industries | array[string] | No | The industry | array from any of the following: "energy", "manufacturing", "commerce", "public", "others" |

The response schema:

| Field | Type | Description | Example |
|:-----|------|------|------|
| calcFactors   | JSON value | The input used for the calculation | |
| calcStatus    | string     | The calculation status | 'S' - successful, 'W'- calculated with some warning, 'E' - not calculated because of error | 
| calcMessages  | array[string] | Warning or error messages
| emissionsPerGas | JSON value | The actual calculation (estimation) of GHG emissions per gas | emission per gas in JSON object |
| totalCO2eq       | number | the total emission in CO2 equivalent | 3423 (unit: kgGHG)
| energyConsumption | number | If applicable, the total energy consumed by the activity, e.g burning the fuel | 234 (unit: MJ)

:::info
Although 'warning' in calcStatus sounds alarming, it can safely be ignored. Warning happens when default data was used instead of more specific one, e.g. EmissionFactor for the 'kr' region was not found hence global data was used.
:::

## Sample request
```sh
curl --request POST 'https://www.ecoloop.one/bapi/ghga/estimate-emissions' \
--header "x-api-key: $ECOLOOP_API_KEY" \
--header 'Content-Type: application/json' \
-d "@activity_data.json" | jq
```

where activity_data.json file is:

```json
{
  "region": "kr",
  "periodStartDt": "2019-03-02",
  "inputAmount": 1000,
  "inputAmountUnitId": "VL-L",
  "activitySourceCode": "0020",
  "activityTypeCode": "2002"
}
```

**FYI**: the `activitySourceCode` "0020" represents "Gas/Diesel Oil" and `activityTypeCode` "2002" represents "combustion/mobile-road"

## Sample response 
```json
{
    "calcFactors": [
        {
            "type": "amount",
            "factorName": "연료 사용량",
            "value": 1000,
            "unit": "",
            "tier": 1
        },
        {
            "type": "efCO2",
            "factorName": "온실가스 배출계수(CO2)",
            "value": 74100,
            "unit": "",
            "tier": 1
        },
        {
            "type": "efCH4",
            "factorName": "온실가스 배출계수(CH4)",
            "value": 3.9,
            "unit": "",
            "tier": 1
        },
        {
            "type": "efN2O",
            "factorName": "온실가스 배출계수(N2O)",
            "value": 3.9,
            "unit": "",
            "tier": 1
        },
        {
            "type": "ncv",
            "factorName": "열량계수(순발열량)",
            "value": 35.2,
            "unit": "",
            "tier": 1
        },
        {
            "type": "gcv",
            "factorName": "열량계수(총발열량)",
            "value": 37.8,
            "unit": "",
            "tier": 1
        },
        {
            "type": "of",
            "factorName": "산화계수",
            "value": 1,
            "unit": "",
            "tier": 1
        },
        {
            "type": "gwpCH4",
            "factorName": "GWP(CH4)",
            "value": 21,
            "unit": ""
        },
        {
            "type": "gwpN2O",
            "factorName": "GWP(N2O)",
            "value": 310,
            "unit": ""
        }
    ],
    "calcStatus": "W",
    "calcMessages": [
        "Calorific calcTier is 2 but EmissionFactor(EF) tier 2 was not found, using EF tier 1."
    ],
    "emissionsPerGas": {
        "CO2": {
            "emission": 2608.32
        },
        "CH4": {
            "emission": 0.13728,
            "CO2eq": 2.88288
        },
        "N2O": {
            "emission": 0.13728,
            "CO2eq": 42.5568
        }
    },
    "totalCO2eq": 2653.75968,
    "energyConsumption": 37800
}
```

Now you can find the applicable codes(`activitySourceCode`, `activityTypeCode`) and do estimations of GHG emission for your activity!
