---
sidebar_position: 7
---

# Congratulations!

You have just learned the **basics of EcoLoop**!

Now you know how to get the necessary codes for the different activities and activity sources, and make requests to emission estimation endpoint.


## What's next?

- Delve into the [Advanced tutorial](../category/tutorial---advanced)
- Build something awesome with the API
