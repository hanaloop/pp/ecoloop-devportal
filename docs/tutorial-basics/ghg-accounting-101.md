---
sidebar_position: 2
tags:
  - GHG Accounting
---

# Greenhouse Gas Accounting 101

Greenhouse Gas Accounting, aka Carbon Accounting, is the process used to measure how much carbon Greenhouse Gas (dioxide equivalents) an organization emits.

While finance accounting records financial transactions, carbon accounting records emission activities and calculates pollutants associated with global warming.

## Calculating Emission

In simple terms the general formula for estimating the GHG emission is:

```
E = A x EF x (1-ER/100)

where:
  - E = emissions,
  - A = activity rate,
  - EF = emission factor, and
  - ER = overall emission reduction efficiency, %

source: https://www3.epa.gov/ttnchie1/ap42/c00s00.pdf
```


Since there are multiple Greenhouse gasses, the industry has opted to come up with a single emission value based on CO2. The other gasses are expressed in therms of CO2 based on the GLobal Warming Potential (GWP).

:::info
GWP is a measure of how much energy the emissions of 1 ton of a gas will absorb over a given period of time, relative to the emissions of 1 ton of carbon dioxide (CO2)
- source: epa.gov
:::

The following is a more concrete example: 

```
CO2 emission = Amount of activity rate * (CO2 EmissionFactor * Heating Value)

CH4's CO2eq emission = (Amount of activity rate * (CH4 EmissionFactor * Heating Value)) * CH4 GWP

N2O's CO2eq emission = (Amount of activity rate * (N2O EmissionFactor * Heating Value)) * N2O GWP
```

:::info
In **EcoLoop**, the following values are sent for calculation/estimation of emission:
- `amount`             - the activity amount (rate), e.g. 45 liters of gasoline 
- `calcTiers`          - the tier (advanced option) needed for calculation parameter
- `activityType`       - the type of activity, e.g. "combustion-stat-solid". 
- `region`             - the country code where activity occurred.  EmissionFactor may differ depending on the region. 
- `activitySourceCode` - the activity's emission source, e.g. 0011 (crude oil) This data is the main filter for choosing the EmissionFactor
- `periodStartDt`      - the time when activity occurred. EmissionFactor may differ depending on the region
- `industries`         - The industry "energy", "manufacturing", "commerce", "public", "others"
:::

## GHG accounting in organizations

Organizations today keep track of their activities in different ways. Many use spreadsheets, some others use dedicated tools such as EcoLoop.

Below is an example of activity tracking sheet

| Emiss  | Facilt | 2007-01 | 2007-02 | 2007-03 | 2007-04 | 2007-05 | 2007-06 |
|--------|:------:|--------:|--------:|--------:|--------:|--------:|--------:|
| gasoline | 0001   | 0.316   | 0.24    | 0.278   | 0.283   | 0.372   | 0.276   |
| diesel   | 0002   | 7.373   | 5.55    | 5.326   | 3.771   | 2.005   | 1.007   |
| elect    | 0003   | 6.628   | 7.295   | 7.786   | 7.026   | 8.929   | 5.674   |
| LNG      | 0004   | 129.591 | 119.821 | 116.259 | 105.576 | 108.625 | 79.795  |
| LPG      | 0005   | 56.774  | 38.878  | 50.117  | 46.895  | 39.223  | 45.293  |
| Heating  | 0006   | 33.343  | 37.017  | 51.49   | 50.802  | 42.492  | 49.068  |

Activity can be in different units, e.g. liters of gasoline, kilogram of 

At the end of the year, organizations makes a summary of the emissions and generates resorts for different purposes:
To submit to regulator such as Emission Trading System (ETS), SEC,
To submit to financial institution as part of due diligence,
To present to trading partners as part of Scope 3 management
To disclose to public in general as part of corporate policy

Different purposes require different levels of detail and certification. For example regulators will ask for a detailed emission report per facility per month. For voluntary disclosure, the level of detail is up to the reporting company.

“Advanced setup” section, will provide more detail on how to setup organization to the level accepted by regulators.
