---
sidebar_position: 3
---

# Fetching Lookup data

Besides the activity rate (amount), there are few reference data that you will need pass as input to the emission calculation

Below is the table with the description of input parameters passed to emission calculation/estimation endpoint: 

| Field | Description | Example |
|:-----|------|------|
| amount | The activity amount (rate) | 34.2 |
| calc Tiers | The tier (advanced option) needed for calculation parameter  | JSON value |
| **activity Type** (lookup) | The type of activity.   | "combustion-stat-solid" |
| region | The country code where activity occurred.  EmissionFactor may differ depending on the region.   | sample |
| **activity Source** (lookup)| The activity's emission source, e.g.  This data is the main filter for choosing the EmissionFactor  | 0011 (crude oil) |
| periodStartDt | The time when activity occurred. EmissionFactor may differ depending on the region  | sample |
| industries | The industry | "energy", "manufacturing", "commerce", "public", "others" |
| **unit** (lookup) | The input unit |  |


The data for parameters in bold `activityType`, `activitySourceCode` and `unit` comes from lookup table.


:::caution
**EcoLoop** is undergoing a major change that will affect the API contract. THe parameter `activityType` and `activitySourceCode` are expected to change in Mid November 2022.
:::


## Retrieving Activity Source from EmissionFactor endpoint

EcoLoop uses the term Activity Source to denote the fuel or material which through an activity produces greenhouse gasses.  

To retrieve `activitySourceCode`, you can use the emissionfactors endpoint to find the appropriate emission factor by name, and get the activitySourceCode from it.

The following example retrieves emissionfactors where id contains the word `gasoline`

```sh
curl --request GET 'https://www.ecoloop.one/bapi/ghga/emissionfactors?id:contains=diesel' \
  --header "x-api-key: $ECOLOOP_API_KEY" \
  --header 'Content-Type: application/json' \
  | jq '[ .[] | { name: .name, region: .region, activityTypeId: .activityTypeId, activitySourceCode: .emissionSourceSink.activitySourceCode, fuel: .emissionSourceSink.fuel, inputAmountUnitId: .calorificDenominatorUnitId} ]'
```

You will get the result similar as below:

```json
[
  {
    "name": "liquid-gas_diesel_oil-1996",
    "region": "*",
    "activityTypeId": "combustion-stat-liquid",
    "activitySourceCode": "0020",
    "fuel": "Gas/Diesel Oil",
    "inputAmountUnitId": "VL-L"
  },
...
  {
    "name": "liquid-gas_diesel_oil-kr-2021",
    "region": "kr",
    "activityTypeId": "combustion-stat-liquid",
    "activitySourceCode": "0020",
    "fuel": "Gas/Diesel Oil",
    "inputAmountUnitId": "VL-L"
  },
  {
    "name": "biomass-liquid_biofuel-biodiesel-1996",
    "region": "*",
    "activityTypeId": "combustion-stat-liquid",
    "activitySourceCode": "0087",
    "fuel": "Liquid Biofuel",
    "inputAmountUnitId": "WG-KG"
  }
]
```


The `activitySourceCode` should be used as the input value for activity source in the calculation endpoint.

Notice in the script above the result of the request was piped to `jq` with additional parameters to only select `name`, `activityTypeId`, `activitySourceCode`, `fuel` and `unitId`. If you remove `jq` pipe, you will get the entire response, which looks like following:

```json
[
  {
    "sid": 18,
    "uid": "27f67f6e-b130-4da7-be19-60c8395e1fcf",
    "createdByUid": "__system__",
    "dateCreated": "2022-12-01T23:55:46.624Z",
    "dateModified": "2022-12-01T23:55:46.624Z",
    "settings": {
      "efNumeratorUnitId": "GS-KG",
      "efDenominatorUnitId": "EN-TJ",
      "activityTypeCategory": "combus-stat",
      "calorificNumeratorUnitId": "EN-MJ",
      "calorificDenominatorUnitId": "VL-L"
    },
    "additionalProps": null,
    "notes": "0020 / liquid-gas_diesel_oil",
    "emissionSourceSinkUid": "e8c38a11-086d-4c0f-b193-f0fbfd0ca11d",
    "erefId": null,
    "activityTypeId": "combustion-stat-liquid",
    "id": "liquid-gas_diesel_oil-1996",
    "name": "liquid-gas_diesel_oil-1996",
    "description": null,
    "region": "*",
    "year": 1996,
    "validStartDt": null,
    "validEndDt": null,
    "lcaActivity": null,
    "factors": {
      "gases": {
        "CH4": {
          "efUnit": "kgCH4/TJ",
          "ef_energy": 3,
          "ef_others": 10,
          "ef_public": 10,
          "ef_commerce": 10,
          "ef_manufacturing": 3
        },
        "CO2": {
          "ef": 74100,
          "efUnit": "kgCO2/TJ"
        },
        "N2O": {
          "efUnit": "kgN2O/TJ",
          "ef_energy": 0.6,
          "ef_others": 0.6,
          "ef_public": 0.6,
          "ef_commerce": 0.6,
          "ef_manufacturing": 0.6
        }
      },
      "netCalorificValue": 43
    },
    "efCO2": 74100,
    "efCH4": 10,
    "efN2O": 0.6,
    "efNumeratorUnitId": "GS-KG",
    "efDenominatorUnitId": "EN-TJ",
    "calorificNumeratorUnitId": "EN-MJ",
    "calorificDenominatorUnitId": "VL-L",
    "typeOfParam": null,
    "measurementTechnique": null,
    "externQualityControl": null,
    "measurementDate": null,
    "sourceOfData": null,
    "dateReleased": null,
    "dataProvider": null,
    "dataCalculated": null,
    "techReference": null,
    "dataQuality": null,
    "dataQualityRef": null,
    "sourceUrl": null,
    "emissionSourceSink": {
      "sid": 11,
      "uid": "e8c38a11-086d-4c0f-b193-f0fbfd0ca11d",
      "createdByUid": "__system__",
      "dateCreated": "2022-12-01T23:55:42.455Z",
      "dateModified": "2022-12-01T23:55:45.515Z",
      "settings": null,
      "additionalProps": null,
      "notes": null,
      "isSink": false,
      "type": "activity",
      "tier": null,
      "id": "liquid-gas_diesel_oil",
      "name": "liquid/gas diesel oil",
      "group": "diesel",
      "description": null,
      "activityCategory": null,
      "activitySector": null,
      "applicableRegions": ["*"],
      "organizationUid": "",
      "activitySourceCode": "0020",
      "sourceSinkCategory": [],
      "unitTypes": ["volume"],
      "fuelType": "liquid",
      "fuelCategory": [
        "liquid",
        "Gas/Diesel Oil"
      ],
      "fuel": "Gas/Diesel Oil",
      "fuelType_nl": null,
      "fuelCategory_nl": [],
      "fuel_nl": null,
      "fuelNatStd": null,
      "fuelMeta": null,
      "techPractices": null,
      "paramConditions": null,
      "abatement": null,
      "otherProperties": null
    }
  },
...
]
```

For now it is sufficient to `activityTypeId`, `activitySourceCode`, and `unitId` and the activity Type code that will retrieve next.

## Retrieving Activity Type from Code endpoint

The emissionfactors endpoint returned you "activityTypeId" but you will actually need the Activity Type Code for a specific activity type.

For the purpose of retrieving Activity Type Code, make the following request:

Where value for `id` in the query string is from `activityTypeId`


```sh
curl --request GET 'https://www.ecoloop.one/bapi/ghga/codes?type=activitytype&_pageSize=100&id=combustion-mobile-road' \
  --header "x-api-key: $ECOLOOP_API_KEY" \
  --header 'Content-Type: application/json' \
  | jq '[ .[] | { type: .type, code: .code, id: .id,  group: .group} ]' 
```


You will get a result similar to sample below:

```json
...
[
  {
    "type": "activitytype",
    "code": "2002",  <-- This is the value for activityTypeCode
    "id": "combustion-mobile-road",
    "group": "combustion-mobile"
  }
]
```

Now you have all the values needed to pass to calc/estimate emissions endpoints.

Next page you will learn how to make a call to estimate emission.

:::info
You can use a [tools page](https://www.ecoloop.one/en/tools/emissionfactors) to browse Codes, Emission Factors and Units.
You need to be logged-in to be able to see the data, otherwise you will see an empty table.
:::

## Summary

To estimate emission you need to get at minimum two reference codes: ActivitySourceCode and ActivityTypeCode.

- **ActivityType** is the the activity that when performed (e.g. burn) together with the *activitySource*, produces GHG emission.
- **ActivitySource** is the fuel or material (e.g. gasoline) that when the activity is performed then GHG emission is produced.
So they go hand in hand.

To find the codes you will need to:

1. Identify the fuel/material by using the `emissionfactor` endpoint, 
  - Search the Emission factors. E.g. gasoline: `https://www.ecoloop.one/bapi/ghga/emissionfactors?id:contains=diesel`
  - Take note of the `activitySourceCode`, `activityTypeId`, and `calorificDenominatorUnitId` (`inputAmountUnitId`) values.
2. Identify activityType by using the `code` enpoint
  - Search the activityType code that matches the value obtained above for `activityTypeId`: `https://www.ecoloop.one/bapi/ghga/codes?id=combustion-stat-liquid`
