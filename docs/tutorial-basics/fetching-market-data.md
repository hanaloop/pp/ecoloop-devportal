---
sidebar_position: 5
---

# Fetching Market Data

EcoLoop also keeps a track of daily Carbon price. At the moment only Korean ETS price is maintained in the system.

You can pull the carbon price with the following request,

Where 
- `region` is the country
- `tradingDate` is the date of the price
- `id` is the good id, in this case `kau23` is the Korean Allowance Unit(KAU) price for 2023

```sh
curl --request GET 'https://www.ecoloop.one/bapi/market/tradingprices?region=kr&tradingDate=2022-11-10&id=kau23' \
--header "x-api-key: $ECOLOOP_API_KEY" \
--header 'Content-Type: application/json' | jq
```
