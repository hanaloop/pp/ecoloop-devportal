---
sidebar_position: 1
---

# Accessing the API

EcoLoop services can be accessed through REST API. The endpoint URLs are based on resources using standard HTTP verbs to create (POST), read (GET), update (PUT), and delete (DELETE). All data accepted as input and returned as output are JSON-encoded.

All connections are made over HTTPS.


To access services through API, you need to pass an API key in the `Authorization` header of the request.

If you do not have an API key yet, you can get it by registering to the system. When you have signed-in to the system, you can click on the account image and go to profile. The API key will be shown in the page.

:::caution
Please keep your API key in a safe place. Do not store it in a publically accessible location. If you believe your API key has been leaked, please notify Ecoloop admin.
:::


## Making your first call

If you have your API key, you can make your first request: retrieve your account details.


```sh
$ export ECOLOOP_API_KEY=<MY_API_KEY_HERE>
$ curl --request GET "https://www.ecoloop.one/bapi/my/account" \
--header "x-api-key: $ECOLOOP_API_KEY" \
--header "Content-Type: application/json" | jq
```

:::info
Notice I first exported `ECOLOOP_API_KEY` as an environment variable and then made a curl call to the API. This will make it easier for subsequent calls.

Also I piped the output to [jq](https://stedolan.github.io/jq/). `jq` is a lightweight command-line JSON processor.

The above commands works on *Nix systems but not in Windows.

If your OS is Windows, I suggest using PowerShell.  You can set environment variables as follows:
$Env:ECOLOOP_API_KEY = MY_API_KEY_HERE' 
:::

If the above command returned a JSON with the details about your account including user information, then congratulations!


If you are getting “guest” user as response, it means you did not use the correct API key or your header was incorrect, make sure you are passing `--header "x-api-key: $ECOLOOP_API_KEY"`

Result when you send wrong API key: 
```
{
  "dateCreated": "2022-11-07T07:39:11.374Z",
  "user": {
    "userCategory": "guest",
    "uid": null,
    "id": null
  }
}
```

If you are having difficulties, please reach out to us tech@hanaloop.com 



## Environments
EcoLoop have two deployment environments: 
- Stage [https://www.ecoloop.one](https://www.ecoloop.one), and
- Production [https://www.ecoloop.io](https:www.ecoloop.io)

The base URL for the Stage API is `https://www.ecoloop.one/bapi`, and for the Production API is `https://www.ecoloop.io/bapi`

The version of Stage may be ahead of Production.

EcoLoop is in continuous improvement. Please use Stage for testing and POC purposes. If you have a feature request, you will likely see it sooner in Stage.

Now that you have made a request, the next step is to understand the domain: carbon accounting.

If you are already familiar with carbon accounting concept, you can skip to “Fetching lookup data” section 

## Tips

Although `curl` is a powerful command-line tool, if you prefer GUI, you can use [Postman](https://www.postman.com/) or [Insomina](https://insomnia.rest/).

Of course you can always go the hard-core way and use any of your programming language of preference.

Back to `curl`, an advantage of command-line tools, especially in unix world, is that you can pipe the output to another tool. Here is where [jq](https://stedolan.github.io/jq/) can be useful as you saw in the example above.
