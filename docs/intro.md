---
sidebar_position: 1
---

# Intro to EcoLoop

Let's discover **EcoLoop**.

EcoLoop is a platform for managing **Greenhouse Gas emissions**.  It takes emission activity and other relevant data as inputs and turns them into insights.

With EcoLoop, users can identify which of their activities are the most carbon intensive, understand why, and pinpoint where they are happening.
Ultimately, this will lead to effective decarbonization of the organization.

## Why is carbon insight needed?

### Your company is a conscious about climate and wants to reduce emission.  
> “You Can’t Manage What You Don’t Measure”

If organization want to reduce their emission, they need to understand where they stand first.

In order to make cost-effective decision on reduction initiatives, you need to collect data analyze which activities are the most carbon intensive. 

### Your company needs to comply with regulations

You need manage carbon emission throughout the year, and at the end of the year present an annual emission report for the regulator.

### Your company needs to satisfy financial institutions

You meed to manage carbon emission and present it to the financial institutions to get investments or loans.

### Your trading partner is request carbon emission

Large companies that buys from you have net-zero policy in their value chain. As one of the supplier your company need to disclose GHG emission report.


## EcoLoop Architecture

Now that we know the purpose of EcoLoop, let's take a quick look at its architecture.

EcoLoop consists of multiple services that expose d APIs.

1. **Auth Service** - Manages users and roles, handles authentication and authorization.
2. **Organization** - Manages the organization and its constituents: sites, facilities, emission sources. The emissions calculated by the GHG Accounting service are associated to the different Organization constituent allowing granular insight of emissions.
3. **GHG Accounting** - Calculates the emission based on input data and *Emission Factors*.
4. **Market** - Provides information about the market. Currently it provides the carbon price of K-ETS.
5. **Data Flow** - The ETL module that enables data integration with external sources. E.g. connection to the electricity company.
6. **Business Intelligence** - Includes analytics, reporting and forecasting.


## Use Cases

### Simple Use Case: Calculation only

A **simple use case** is to browse emission factors and make a GHG emission calculation.

:::info
Simple calculation API will not store any data in EcoLoop server.
:::

Example of this use case is an ERP system integrating with EcoLoop for just emission calculation. The ERP system handles all the storing and interaction with the user.


### Advanced Use Case: Full Organization Setup

A more **advanced use case** is to set up the organization constituents: *sites*, *facilities* within sites and *emission sources* within facilities.

Instead of calculating the emission, the application will add `ActivityLog`s to EcoLoop, and EcoLoop will take care of calculating, and storing the results.

:::info
When emissions are associated with Organization constituents, it is possible to run analytics, reports and forecasts.
:::


## Getting Started

If your intention is to use EcoLoop API, you need to obtain the API key.

To obtain an API key, [sign-up to EcoLoop](./how-tos#how-do-i-sign-up), and retrieve the API key which is shown as **Account UID**.

Upon registration, you will be given minimal permission until your account is verified.

:::info
Send an email to `admin@ecoloop.io` requesting promotion of your account as "verified", include your email or Account UID in the request.
:::

Once verified, you can create an organization and its constituent using the REST calls with API key and/or interacting with web interface [ecoloop.one](http://ecoloop.one).

Example of a curl command using API key:
```sh
curl --request GET 'https://www.ecoloop.one/bapi/authz/resourceroles' \
--header "x-api-key: $ECOLOOP_API_KEY" \
--header 'Content-Type: application/json' 
```


:::note
Dev environment (www.ecoloop.one) does not require an API key for some of the endpoints. 
:::
