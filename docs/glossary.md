---
sidebar_position: 6
---

# Glossary

- **Greenhouse Gas**

    The gases that retains heat in the atmosphere. There are many gasses that are considered GHG but the main one is Carbon dioxide. 

- **Emission Factor**

- **Calorific Value**

- **Emission Source Sink**

- **Activity Source**

- **Activity Type**

- **Korea's Carbon Credit Units** Korean Credit Unit (KCU), Korean Allowance Unit(KAU)